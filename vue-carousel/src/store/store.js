import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
  showOverlay: false,
  overlayImageUrl: '',
  maxImages: 4,
  galleryRange: {
    minRange: 0,
    maxRange: 4,
  },
  galleryContent: []
  },
  getters: {
    getOverlayState(state) {
      return {
        showOverlay: state.showOverlay,
        overlayImageUrl: state.overlayImageUrl
      }
    }, 
    getGalleryCollection(state) {
      return state.galleryContent;
    },
    getGalleryCollectionCount(state) {
      return state.galleryContent.length;
    },
    contentLoaded(state) {
      return state.galleryContent.length > 0 ? true : false;
    },
    getMinRange(state) {
      return state.galleryRange.minRange;
    },
    getMaxRange(state) {
      return state.galleryRange.maxRange;
    }
  },
  mutations: {
    setOverlayOpen(state, imageUrl) {
      state.showOverlay = !state.showOverlay;
      state.overlayImageUrl = imageUrl;
    },
    setBackwardCount(state) {
      if (state.galleryRange.minRange !== 0) {
        state.galleryRange.minRange--;
        state.galleryRange.maxRange--;
      }
    },
    setForwardCount(state) {
      if (state.galleryRange.maxRange <= state.galleryContent.length && state.galleryContent.length > 0) {
        state.galleryRange.minRange++;
        state.galleryRange.maxRange++;
      }
    },
    fetchApi(state, searchObj) {
      let searchVal
      if (searchObj) {
        searchVal = searchObj.searchVal.trim();
      } else {
        return
      }

      fetch('http://localhost:3333/get-pics', {
        method: 'POST',
        body: JSON.stringify({search: searchVal}),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        }
      })
        .then(response => {
          if (response.status < 200 && response.status > 300) {
            throw Error(response.statusText);
          }
          return response.json();
        })
        .then(response => {
          let res = JSON.parse(response)
          state.galleryContent = res.hits;
        })
        .catch(() => {
          state.galleryContent = [];
        });
    }
  },
  actions: {
    searchImage({commit}, searchObj) {
      commit('fetchApi', { searchVal: searchObj.searchVal });
    }
  }
});