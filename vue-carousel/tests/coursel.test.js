import { shallowMount } from '@vue/test-utils';
import Carousel from './src/components/Carousel'

describe('Coursel template', () => {

  it('Renders Carousel component', () => {
    const wrapper = shallowMount(Carousel);
    expect(wrapper.find("h1")).toMach("Carousel Test");
  })
})